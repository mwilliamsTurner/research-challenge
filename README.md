# research-dev-challenge - a developer challenge for the research team

This challenge is being used by the research team to evaluate candidates.  It is derived from angular-seed 
[https://github.com/angular/angular-seed](https://github.com/angular/angular-seed).

## Getting Started

To get started simply clone the research-dev-challenge repository and install the dependencies:

### Clone research-dev-challenge

```
git clone https://github.com/angiesmith1/research-dev-challenge.git
cd research-dev-challenge
```

### Install Dependencies

We have two kinds of dependencies in this project: tools and angular framework code.  The tools help
us manage and test the application.

* We get the tools we depend upon via `npm`, the [node package manager][npm].
* We get the angular code via `bower`, a [client-side code package manager][bower].

We have preconfigured `npm` to automatically run `bower` so we can simply do:

```
npm install
```

Behind the scenes this will also call `bower install`.  You should find that you have two new
folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `app/bower_components` - contains the angular framework files

*Note that the `bower_components` folder would normally be installed in the root folder but
angular-seed changes this location through the `.bowerrc` file.  Putting it in the app folder makes
it easier to serve the files by a webserver.*

### Run the Application

We have preconfigured the project with a simple development web server.  The simplest way to start
this server is:

```
npm start
```

Now browse to the app at `http://localhost:8000/`.

## Directory Layout

```
app/                    --> all of the source files for the application
  app.min.css           --> default stylesheet
  integers/                --> the integers view template and logic
    integers.html            --> the partial template
    integers.js              --> the controller logic
    integers_test.js         --> tests of the controller
  app.js                --> main application module
  index.html            --> app layout file (the main html template file of the app)
  index-async.html      --> just like index.html, but loads js files asynchronously
karma.conf.js         --> config file for running unit tests with Karma
```

### Running Unit Tests

The unit tests are written in [Jasmine][jasmine], which we run with the [Karma Test Runner][karma]. We provide a
 Karma configuration file to run them.

* the configuration is found at `karma.conf.js`
* the unit tests are found next to the code they are testing and are named as `..._test.js`.

The easiest way to run the unit tests is to use the supplied npm script:

```
npm test
```

Matt:  I would suggest just give one way to run unit tests.  Probably the command below.

This script will start the Karma test runner to execute the unit tests. Moreover, Karma will sit and
watch the source and test files for changes and then re-run the tests whenever any of them change.
This is the recommended strategy; if your unit tests are being run every time you save a file then
you receive instant feedback on any changes that break the expected code functionality.

You can also ask Karma to do a single run of the tests and then exit.  This is useful if you want to
check that a particular version of the code is operating as expected.  The project contains a
predefined script to do this:

```
npm run test-single-run
```

Matt:  I suggest putting something here and state:  Now, go to the website and follow the instructions from there.... with the localhost url.
