'use strict';

angular.module('myApp.integers', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/integers', {
    templateUrl: 'integers/integers.html',
    controller: 'integersCtrl'
  });
}])

.controller('integersCtrl', ['$scope', function($scope) {
  var output = 'No integer found!'; 
  $scope.input = '';

  $scope.submit = function(str) {
    // str.split(',');
    var stringArray = str.split(',');
    var biggestInt = -999999999;
    for (var i = 0; i < stringArray.length; i++) { 
      try {
        var x = parseInt(stringArray[i]);
        if (!isNaN(x)) {
          if (x > biggestInt) { 
            biggestInt = x;
          }
        }
      } 
      catch (e){
        return;
      }
    }
    if (biggestInt === -999999999) { 
       $scope.result = 'No Integers Found';
    }
    else {
      $scope.result = biggestInt;
    }
  }
  
  // 10, 3, 232, ,3000, 120, B, 51, 6, 7, 120, 10, 10
  
}]);